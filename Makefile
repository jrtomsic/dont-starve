help:
	@echo
	@echo "Makefile Targets:"
	@echo "   help: this text"
	@echo "   clean: build cli curses binary"
	@echo

clean:
	find . -iname .ds_store -delete

FORCE:
