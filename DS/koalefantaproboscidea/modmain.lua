--[[
Koalefanta Proboscidea
Version: 0.3

Made by Q and Raccoon Superhero

The mod gives you four special koalefants you can discover in your lands by following their footprints.
Each of them are different in appearance and in personality.
These koalefants are stronger in health and gives you less meat when killed, instead you can get other useful loot.

You can have five koalefants in your world at the same time.

----------------------------------------------------------------------------------------------------------------------
Information about the four koalefants:

Feathered Koalefant:
Hunt: Grassland
Periodically drops: jet feather and crimson feather
Health: 300
Drops when killed: 4 meats, 1 light trunk, 4 crimson feathers, 3 jet feathers, 2 azure feathers 
Extra: Coward

Forest Koalefant:
Hunt: Summer, dusk
Periodically drops: flower and fireflies
Health: 550
Drops when killed: 4 meats, 1 light trunk, 4 fireflies, 3 green mushrooms

Rocky Koalefant:
Hunt: Rocky biome
Periodically drops: rocks and flint
Health: 800
Drops when killed: 4 meats, thick trunk, 6 rocks, 3 marbles
Extra: Rock-headed

Nightmare Koalefant:
Hunt: Night time
Periodically drops: evil flowers and evil flower petals
Health: 700
Drops when killed: 4 monster meats, 1 thick trunk, 4 nightmare fuels
Extra: Makes you insane
--]]

STRINGS = GLOBAL.STRINGS
GLOBAL.STRINGS.NAMES.TRUNK_WINTER = "Thick Trunk"

STRINGS = GLOBAL.STRINGS
GLOBAL.STRINGS.NAMES.TRUNK_SUMMER = "Light Trunk"

GLOBAL.STRINGS.NAMES.KOALEFANT_FEATHERED = "Feathered Koalefant"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KOALEFANT_FEATHERED = "I would be impressed if he could fly away"

GLOBAL.STRINGS.NAMES.KOALEFANT_ROCKY = "Rocky Koalefant"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KOALEFANT_ROCKY = "He looks rather angry"

GLOBAL.STRINGS.NAMES.KOALEFANT_FOREST = "Forest Koalefant"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KOALEFANT_FOREST = "The colour of his coat hides him well"

GLOBAL.STRINGS.NAMES.KOALEFANT_NIGHTMARE = "Nightmare Koalefant"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KOALEFANT_NIGHTMARE = "Something is odd about this one"

GLOBAL.STRINGS.NAMES.KOALEFANT_ALBINO = "Albino Koalefant"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KOALEFANT_ALBINO = "His eyes are bad"

TUNING.KOALEFANTSPECIAL_TARGET_DIST = 20
TUNING.KOALEFANTSPECIAL_CHASE_DIST = 30

PrefabFiles = {
    "koalefantspecial"
}

-- Assets = {
--     Asset("ANIM", "anim/koalefant_feathered_build.zip"),
--     Asset("ANIM", "anim/koalefant_rocky_build.zip"),
--     Asset("ANIM", "anim/koalefant_forest_build.zip"),
--     Asset("ANIM", "anim/koalefant_nightmare_build.zip"),
--     Asset("ANIM", "anim/koalefant_albino_build.zip"),
-- }

AddComponentPostInit("hunter", function(self)
	self.beast_prefab_feather = "koalefant_feathered"
    self.beast_prefab_forest = "koalefant_forest"
    self.beast_prefab_nightmare = "koalefant_nightmare"
    self.beast_prefab_rocky = "koalefant_rocky"
	self.beast_prefab_albino = "koalefant_albino"

    function self:SpawnHuntedBeast()
        local pt = GLOBAL.Vector3(GLOBAL.GetPlayer().Transform:GetWorldPosition())
        local spawn_pt = self:GetSpawnPoint(pt, TUNING.HUNT_SPAWN_DIST)
        if not spawn_pt then return false end

        self.huntedbeast = self:SpawnBeastPrefab(spawn_pt)
        if not self.huntedbeast then return false end

        self.huntedbeast.Physics:Teleport(spawn_pt:Get())
        self.inst:ListenForEvent("death", function(inst, data) self:OnBeastDeath(self.huntedbeast) end, self.huntedbeast)

        return true
    end

    function self:SpawnBeastPrefab(spawn_pt)
        local prefabs = {}
        local world = GLOBAL.GetWorld()

        if GLOBAL.IsDLCEnabled(1) then
            if math.random() <= self:GetAlternateBeastChance() then
                local prefab = GLOBAL.SpawnPrefab(self.alternate_beast_prefab)
                prefab.components.combat:SetTarget(GLOBAL.GetPlayer())
                return prefab
            end
        end

        if world.components.seasonmanager:IsWinter() then
            table.insert(prefabs, self.beast_prefab_winter)
        else
            table.insert(prefabs, self.beast_prefab_summer)
        end

	    if world.components.clock:IsNight() then
            table.insert(prefabs, self.beast_prefab_nightmare)
            table.insert(prefabs, self.beast_prefab_albino)
        end
		
	    if world.Map:GetTileAtPoint(spawn_pt.x, spawn_pt.y, spawn_pt.z) == GLOBAL.GROUND.ROCKY then
            table.insert(prefabs, self.beast_prefab_rocky)
        end

     	if world.Map:GetTileAtPoint(spawn_pt.x, spawn_pt.y, spawn_pt.z) == GLOBAL.GROUND.GRASS then
            table.insert(prefabs, self.beast_prefab_feather)
        end
		
		if world.Map:GetTileAtPoint(spawn_pt.x, spawn_pt.y, spawn_pt.z) == GLOBAL.GROUND.FOREST then
            table.insert(prefabs, self.beast_prefab_forest)
        end

        return GLOBAL.SpawnPrefab(prefabs[math.random(#prefabs)])
    end
end)
