name = "Koalefanta Proboscidea"
description = "More Koalefants to discover!\n\nIdea and original code by Raccoon and _Q_.\nCleanup and tweaks by Plaidman."
author = "Plaidman "
version = "6p"

forumthread = "/files/"

api_version = 6

icon_atlas = "modicon.xml"
icon = "modicon.tex"

dont_starve_compatible = true
reign_of_giants_compatible = true
