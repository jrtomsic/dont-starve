local function DoSpawn(inst)
    local spawner = inst.components.randomperiodicspawner

    if spawner then
		spawner.target_time = nil    
		spawner:TrySpawn()
        spawner:Start()
    end
end

local function DoSpawnOne(inst)
    local spawner = inst.components.randomperiodicspawner

    if spawner then
		spawner.target_time = nil    
		spawner:TrySpawnOne()
        spawner:Start()
    end
end

local RandomPeriodicSpawner = Class(function(self, inst)
    self.inst = inst
    self.basetime = 40
    self.randtime = 60
	
    self.prefab = nil
    self.prefab2 = nil
	
    self.range = nil
    self.density = nil
    self.spacing = nil
    
    self.onspawn = nil
    self.spawntest = nil
    
    self.spawnoffscreen = false
end)

function RandomPeriodicSpawner:SetPrefab(prefab)
    self.prefab = prefab
end

function RandomPeriodicSpawner:SetPrefabs(prefab, prefab2)
    self.prefab = prefab
	self.prefab2 = prefab2
end

function RandomPeriodicSpawner:SetRandomTimes(basetime, variance)
    self.basetime = basetime
    self.randtime = variance
end

function RandomPeriodicSpawner:SetDensityInRange(range, density)
    self.range = range
    self.density = density
end

function RandomPeriodicSpawner:SetMinimumSpacing(spacing)
    self.spacing = spacing
end

function RandomPeriodicSpawner:SetOnlySpawnOffscreen(offscreen)
    self.spawnoffscreen = offscreen
end

function RandomPeriodicSpawner:SetOnSpawnFn(fn)
    self.onspawn = fn
end

function RandomPeriodicSpawner:SetSpawnTestFn(fn)
    self.spawntest = fn
end

function RandomPeriodicSpawner:TrySpawn(prefab, prefab2)
    local var = math.random(1,2)

	if var == 1 then 
        prefab = prefab or self.prefab
	elseif var == 2 then 
        prefab = prefab2 or self.prefab2
	end

    if not self.inst:IsValid() or not prefab or prefab2 then
        return
    end
    
    local canspawn = true
    
    if self.range or self.spacing then
        local pos = Vector3(self.inst.Transform:GetWorldPosition())
        local ents = TheSim:FindEntities(pos.x,pos.y,pos.z, self.range or self.spacing)
        local count = 0

        for k,v in pairs(ents) do
            if v.prefab == prefab or v.prefab == prefab2 then
                if self.spacing and v:GetDistanceSqToInst(self.inst) < self.spacing*self.spacing then
                    canspawn = false
                    break
                end
                count = count + 1
            end
        end

        if self.density and count >= self.density then
            canspawn = false
        end
    end
    
    if self.spawnoffscreen and not self.inst:IsAsleep() then
        canspawn = false
    end
    
    if self.spawntest then
        canspawn = canspawn and self.spawntest(self.inst)
    end

    if canspawn then
        local inst = SpawnPrefab(prefab)

        if self.onspawn then
            self.onspawn(self.inst, inst)
        end

        inst.Transform:SetPosition(self.inst.Transform:GetWorldPosition())
    end

    return canspawn
end

function RandomPeriodicSpawner:TrySpawnOne(prefab)
    prefab = prefab or self.prefab

    if not self.inst:IsValid() or not prefab then
        return
    end
    
    local canspawn = true
    
    if self.range or self.spacing then
        local pos = Vector3(self.inst.Transform:GetWorldPosition())
        local ents = TheSim:FindEntities(pos.x,pos.y,pos.z, self.range or self.spacing)
        local count = 0

        for k,v in pairs(ents) do
            if v.prefab == prefab then
                if self.spacing and v:GetDistanceSqToInst(self.inst) < self.spacing*self.spacing then
                    canspawn = false
                    break
                end
                count = count + 1
            end
        end

        if self.density and count >= self.density then
            canspawn = false
        end
    end
    
    if self.spawnoffscreen and not self.inst:IsAsleep() then
        canspawn = false
    end
    
    if self.spawntest then
        canspawn = canspawn and self.spawntest(self.inst)
    end

    if canspawn then
        local inst = SpawnPrefab(prefab)

        if self.onspawn then
            self.onspawn(self.inst, inst)
        end

        inst.Transform:SetPosition(self.inst.Transform:GetWorldPosition())
    end

    return canspawn
end

function RandomPeriodicSpawner:Start()
    local t = self.basetime + math.random()*self.randtime
    self.target_time = GetTime() + t

	if self.prefab2 == nil then 
        self.task = self.inst:DoTaskInTime(t, DoSpawnOne)
	else
        self.task = self.inst:DoTaskInTime(t, DoSpawn)
	end
end


function RandomPeriodicSpawner:Stop()
    self.target_time = nil

    if self.task then
        self.task:Cancel()
        self.task = nil
    end
end

function RandomPeriodicSpawner:LongUpdate(dt)
	if self.target_time then
		if self.task then
			self.task:Cancel()
			self.task = nil
		end

		local time_to_wait = self.target_time - GetTime() - dt
		
		if time_to_wait <= 0 then
			if self.prefab2 == nil then 
                DoSpawnOne(self.inst)
			else
                DoSpawn(self.inst)
			end			
		else
			self.target_time = GetTime() + time_to_wait

			if self.prefab2 == nil then 
                self.task = self.inst:DoTaskInTime(time_to_wait, DoSpawnOne)
			else
                self.task = self.inst:DoTaskInTime(time_to_wait, DoSpawn)
			end	
		end
	end
end

return RandomPeriodicSpawner
