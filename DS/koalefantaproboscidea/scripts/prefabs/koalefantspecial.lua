local brain = require "brains/koalefantbrain"
local coward = require "brains/cowardbrain"
local brave = require "brains/bravebrain"

local assets = {
    Asset("ANIM", "anim/koalefant_feathered_build.zip"),
	Asset("ANIM", "anim/koalefant_rocky_build.zip"),
	Asset("ANIM", "anim/koalefant_forest_build.zip"),
	Asset("ANIM", "anim/koalefant_nightmare_build.zip"),
	Asset("ANIM", "anim/koalefant_albino_build.zip"),
	Asset("ANIM", "anim/koalefant_basic.zip"),
    Asset("ANIM", "anim/koalefant_actions.zip"),
}

local prefabs = {
    "meat",
    "feather_robin",
	"feather_crow",
	"feather_robin_winter",
    "trunk_summer",
	"trunk_winter",
	"fireflies",
	"green_cap",
	"nightmarefuel",
	"monstermeat",
	"poop",
	"marble"
}

local loot_feathered = {
    "meat", "meat", "meat", "meat",
    "feather_robin", "feather_robin", "feather_robin", "feather_robin",
    "feather_crow", "feather_crow", "feather_crow",
    "feather_robin_winter", "feather_robin_winter",
    "trunk_summer"
}

local loot_rocky = {
    "meat", "meat", "meat", "meat",
    "rocks", "rocks", "rocks", "rocks", "rocks", "rocks",
    "marble", "marble", "marble",
    "trunk_winter"
}

local loot_forest = {
    "meat", "meat", "meat", "meat",
    "blue_cap", "blue_cap", "blue_cap", "blue_cap",
    "fireflies", "fireflies", "fireflies",
    "trunk_summer"
}

local loot_nightmare = {
    "monstermeat", "monstermeat", "monstermeat", "monstermeat",
    "beardhair", "beardhair", "beardhair", "beardhair",
    "nightmarefuel", "nightmarefuel", "nightmarefuel",
    "trunk_winter"
}

local loot_albino = {
    "meat", "meat", "meat", "meat",
    "ash", "ash", "ash", "ash", "ash", "ash",
    "lightbulb", "lightbulb", "lightbulb",
    "trunk_summer"
}

local WAKE_TO_RUN_DISTANCE = 10
local SLEEP_NEAR_ENEMY_DISTANCE = 14

local function ShouldWakeUp(inst)
    return DefaultWakeTest(inst) or inst:IsNear(GetPlayer(), WAKE_TO_RUN_DISTANCE)
end

local function ShouldSleep(inst)
    return DefaultSleepTest(inst) and not inst:IsNear(GetPlayer(), SLEEP_NEAR_ENEMY_DISTANCE)
end

local function retargetfn(inst)
    local dist = TUNING.KOALEFANTSPECIAL_TARGET_DIST
    return FindEntity(inst, dist, function(guy) 
		return  not guy:HasTag("koalefant") and inst.components.combat:CanTarget(guy) --not guy:HasTag("wall") and
    end)
end

local function OnNewTarget(inst, data) end
local function GetStatus(inst) end
local function Retarget(inst) end

local function KeepTarget(inst, target)
    return distsq(Vector3(target.Transform:GetWorldPosition() ), Vector3(inst.Transform:GetWorldPosition() ) ) < TUNING.KOALEFANTSPECIAL_CHASE_DIST * TUNING.KOALEFANTSPECIAL_CHASE_DIST
end

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30,function(dude)
        return dude:HasTag("koalefant") and not dude:HasTag("player") and not dude.components.health:IsDead()
    end, 5)
end

local function CalcSanityAura(inst, observer)
	if inst.components.combat.target then
        return -TUNING.SANITYAURA_LARGE
	else
        return -TUNING.SANITYAURA_MED
	end

	return 0
end

local function create_base(sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local sound = inst.entity:AddSoundEmitter()
	local shadow = inst.entity:AddDynamicShadow()
	shadow:SetSize( 4.5, 2 )
    inst.Transform:SetFourFaced()

    MakeCharacterPhysics(inst, 100, .75)
    
    inst:AddTag("koalefant")
    anim:SetBank("koalefant")
    anim:PlayAnimation("idle_loop", true)
    
    inst:AddTag("animal")
    inst:AddTag("largecreature")

    inst:AddComponent("eater")
    inst.components.eater:SetVegetarian()
    
    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "beefalo_body"
    inst.components.combat:SetDefaultDamage(TUNING.KOALEFANT_DAMAGE)
 
    inst:AddComponent("health")

    inst:AddComponent("lootdropper")
	
    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus
    
    inst:AddComponent("knownlocations")
    
    MakeLargeBurnableCharacter(inst, "beefalo_body")
    MakeLargeFreezableCharacter(inst, "beefalo_body")

    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = 1.5
    inst.components.locomotor.runspeed = 7
    
    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper:SetSleepTest(ShouldSleep)
    inst.components.sleeper:SetWakeTest(ShouldWakeUp)
    
    inst:SetStateGraph("SGkoalefant")
	
    return inst
end

local function create_feathered(sim)
	local inst = create_base(sim)
	inst:SetBrain(coward)
	
	inst:AddComponent("attackmemory")

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()

	inst.AnimState:SetBuild("koalefant_feathered_build")
    inst.components.lootdropper:SetLoot(loot_feathered)
	inst.components.health:SetMaxHealth(300)

	inst:AddComponent("randomperiodicspawner")
    inst.components.randomperiodicspawner:SetPrefabs("feather_crow", "feather_robin")
    inst.components.randomperiodicspawner:SetRandomTimes(45, 60)
    inst.components.randomperiodicspawner:SetDensityInRange(20, 1)
    inst.components.randomperiodicspawner:SetMinimumSpacing(8)
    inst.components.randomperiodicspawner:Start()
	
	return inst
end

local function create_rocky(sim)
	local inst = create_base(sim)
	inst:SetBrain(brave)

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()

	inst:AddComponent("randomperiodicspawner")
    inst.components.randomperiodicspawner:SetPrefabs("flint", "rocks")
    inst.components.randomperiodicspawner:SetRandomTimes(65, 80)
    inst.components.randomperiodicspawner:SetDensityInRange(20, 1)
    inst.components.randomperiodicspawner:SetMinimumSpacing(8)
    inst.components.randomperiodicspawner:Start()

	inst.AnimState:SetBuild("koalefant_rocky_build")
    inst.components.lootdropper:SetLoot(loot_rocky)
	inst.components.health:SetMaxHealth(800)
	inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("attacked", function(inst, data) OnAttacked(inst, data) end)
	inst.components.combat:SetRetargetFunction(3, retargetfn)

	inst.components.locomotor.walkspeed = 1
    inst.components.locomotor.runspeed = 5

	return inst	
end

local function create_forest(sim)
	local inst = create_base(sim)
	inst:SetBrain(brain)

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()

	inst:AddComponent("randomperiodicspawner")
    inst.components.randomperiodicspawner:SetPrefab("flower", "pinecone")
    inst.components.randomperiodicspawner:SetRandomTimes(65, 80)
    inst.components.randomperiodicspawner:SetDensityInRange(20, 1)
    inst.components.randomperiodicspawner:SetMinimumSpacing(8)
    inst.components.randomperiodicspawner:Start()

	inst.AnimState:SetBuild("koalefant_forest_build")
    inst.components.lootdropper:SetLoot(loot_forest)
	inst.components.health:SetMaxHealth(550)
	inst.components.combat:SetRetargetFunction(1, Retarget)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("attacked", function(inst, data) OnAttacked(inst, data) end)

	return inst
end

local function create_nightmare(sim)
	local inst = create_base(sim)
	inst:SetBrain(brain)

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()

	inst:AddComponent("randomperiodicspawner")
    inst.components.randomperiodicspawner:SetPrefab("flower_evil", "beardhair")
    inst.components.randomperiodicspawner:SetRandomTimes(75, 90)
    inst.components.randomperiodicspawner:SetDensityInRange(20, 2)
    inst.components.randomperiodicspawner:SetMinimumSpacing(8)
    inst.components.randomperiodicspawner:Start()

	inst.AnimState:SetBuild("koalefant_nightmare_build")
    inst.components.lootdropper:SetLoot(loot_nightmare)
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura
	inst.components.health:SetMaxHealth(700)
	inst.components.combat:SetRetargetFunction(1, Retarget)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("attacked", function(inst, data) OnAttacked(inst, data) end)
	inst.components.sleeper.nocturnal = true

	return inst
end

local function create_albino(sim)
	local inst = create_base(sim)
	inst:SetBrain(brain)

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("guano")
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()

	inst:AddComponent("randomperiodicspawner")
    inst.components.randomperiodicspawner:SetPrefab("ash", "silk")
    inst.components.randomperiodicspawner:SetRandomTimes(75, 90)
    inst.components.randomperiodicspawner:SetDensityInRange(20, 2)
    inst.components.randomperiodicspawner:SetMinimumSpacing(8)
    inst.components.randomperiodicspawner:Start()

	inst.AnimState:SetBuild("koalefant_albino_build")
    inst.components.lootdropper:SetLoot(loot_albino)
	inst.components.health:SetMaxHealth(600)
	inst.components.combat:SetRetargetFunction(1, Retarget)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("attacked", function(inst, data) OnAttacked(inst, data) end)
	inst.components.sleeper.nocturnal = true

    local light = inst.entity:AddLight()
    light:SetFalloff(0.4)
    light:SetIntensity(0.7)
    light:SetRadius(1)
    light:SetColour(237/255, 237/255, 209/255)
    light:Enable(true)

	return inst
end

return Prefab( "forest/animals/koalefant_feathered", create_feathered, assets, prefabs),
	   Prefab( "forest/animals/koalefant_rocky", create_rocky, assets, prefabs),
	   Prefab( "forest/animals/koalefant_forest", create_forest, assets, prefabs),
	   Prefab( "forest/animals/koalefant_nightmare", create_nightmare, assets, prefabs),
	   Prefab( "forest/animals/koalefant_albino", create_albino, assets, prefabs)
