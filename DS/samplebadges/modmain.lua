require = GLOBAL.require
local Badge = require "widgets/badge"
local Text = require "widgets/text"

local sampleNumber = 0
local maxNumber = 100

-- define the sample badge class
local SampleBadge = Class(Badge, function(self, owner)
	Badge._ctor(self, "health", owner)
end)

AddSimPostInit(function()
	local player = GLOBAL.GetPlayer()
	local status = player.HUD.controls.status

    -- instantiate the sample badge object
    status.sampleBadge = status:AddChild(SampleBadge(owner))
    status.sampleBadge:SetHAnchor(GLOBAL.ANCHOR_MIDDLE)
    status.sampleBadge:SetVAnchor(GLOBAL.ANCHOR_TOP)
    status.sampleBadge:SetPosition(0,-50,0)
    status.sampleBadge:SetScaleMode(GLOBAL.SCALEMODE_PROPORTIONAL)

    player:DoPeriodicTask(0.5, function(inst)
        sampleNumber = sampleNumber + 1

        if sampleNumber > maxNumber then
            sampleNumber = 0
        end

        status.sampleBadge:SetPercent(sampleNumber/maxNumber, maxNumber)
    end)
end)

AddSimPostInit(function()
	local player = GLOBAL.GetPlayer()
	local status = player.HUD.controls.status

    -- instantiate the sample badge object
    status.moistureBadge = status:AddChild(SampleBadge(owner))
    status.moistureBadge:SetHAnchor(GLOBAL.ANCHOR_MIDDLE)
    status.moistureBadge:SetVAnchor(GLOBAL.ANCHOR_TOP)
    status.moistureBadge:SetPosition(100,-50,0)
    status.moistureBadge:SetScaleMode(GLOBAL.SCALEMODE_PROPORTIONAL)

    player:DoPeriodicTask(0.5, function(inst)
        local season = GLOBAL.GetSeasonManager()
        local moisture = season.atmo_moisture
        local limit = season.moisture_limit

        status.moistureBadge:SetPercent(moisture/limit, limit)
    end)
end)

AddSimPostInit(function()
	local player = GLOBAL.GetPlayer()
	local status = player.HUD.controls.status

    -- instantiate the sample badge object
    status.naughtyBadge = status:AddChild(SampleBadge(owner))
    status.naughtyBadge:SetHAnchor(GLOBAL.ANCHOR_MIDDLE)
    status.naughtyBadge:SetVAnchor(GLOBAL.ANCHOR_TOP)
    status.naughtyBadge:SetPosition(-100,-50,0)
    status.naughtyBadge:SetScaleMode(GLOBAL.SCALEMODE_PROPORTIONAL)

    player:DoPeriodicTask(0.5, function(inst)
        local naughtiness = player.components.kramped.actions
        local limit = player.components.kramped.threshold or 30

        status.naughtyBadge:SetPercent(naughtiness/limit, limit)
    end)
end)
