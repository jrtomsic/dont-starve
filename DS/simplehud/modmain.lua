require = GLOBAL.require
local Text = require "widgets/text"

local configDisplayTemperature = GetModConfigData("displayTemperature")
local configNumberAlpha = GetModConfigData("numberAlpha")
local configDeltaColorChange = GetModConfigData("deltaColorChange")

local white = {1,1,1}
local red = {1,0,0}
local green = {0,1,0}

AddClassPostConstruct("widgets/badge", function(self)
    self.num:SetColour(white[1], white[2], white[3], configNumberAlpha)
    self.num:Show()

    self.num2 = self:AddChild(Text(GLOBAL.BODYTEXTFONT, 33))
    self.num2:SetHAlign(GLOBAL.ANCHOR_MIDDLE)
    self.num2:SetPosition(5, 0, 0)
    self.num2:SetString("")
    self.num2:SetColour(white[1], white[2], white[3], configNumberAlpha)
    self.num2:Hide()

    self.deltaColorChange = configDeltaColorChange

    local oldOnLoseFocus = self.OnLoseFocus
    function self:OnLoseFocus()
        local result = oldOnLoseFocus(self)

        self.num:Show()
        self.num2:Hide()

        return result
    end

    local oldOnGainFocus = self.OnGainFocus
    function self:OnGainFocus()
        local result = oldOnGainFocus(self)

        if self.num2:GetString() ~= "" then
            self.num:Hide()
            self.num2:Show()
        end

        return result
    end

    local oldSetPercent = self.SetPercent
    function self:SetPercent(val, max)
        self:SetDeltaColor(self.percent, val)

        local result = oldSetPercent(self, val, max)

        if self.num.shown ~= true and self.num2.shown ~= true then
            self.num:Show()
        end

        self.num2:SetString(tostring(math.floor(max or 100)))

        return result
    end

    function self:SetDeltaColor(old, new)
        if old == new then return end

        if self.deltaColorChange == "no" then return end

        if self.deltaColorChange == "notHunger" then
            if self.simpleHudHungerOverride == true then return end
        end

        local newColor = white
        if old < new then
            newColor = green
        elseif old > new then
            newColor = red
        end

        self.num:SetColour(newColor[1], newColor[2], newColor[3], configNumberAlpha)
        self.num2:SetColour(newColor[1], newColor[2], newColor[3], configNumberAlpha)

        if self.colorTask ~= nil then
            self.colorTask:Cancel()
        end

        self.colorTask = self.inst:DoTaskInTime(.5, function(inst)
            self.num:SetColour(white[1], white[2], white[3], configNumberAlpha)
            self.num2:SetColour(white[1], white[2], white[3], configNumberAlpha)
        end)
    end
end)

AddClassPostConstruct("widgets/hungerbadge", function(self)
    self.simpleHudHungerOverride = true
end)

-- Temperature in the clock area
if configDisplayTemperature == true then
    AddClassPostConstruct("widgets/uiclock", function(self)
        self.text:SetPosition(5, 3/self.base_scale, 0)

        self.text2 = self:AddChild(Text(GLOBAL.BODYTEXTFONT, 25/self.base_scale))
        self.text2:SetPosition(5, -23/self.base_scale, 0)
        self.text2:SetColour(white[1], white[2], white[3], configNumberAlpha)
        self.text2:Show()

        self.inst:DoPeriodicTask(0.25, function(inst)
            local temperature = GLOBAL.GetPlayer().components.temperature.current or "?"

            if temperature ~= "?" then
                temperature = tostring(math.floor(temperature))
            end

            self.text2:SetString(temperature .. "\176C")
        end)
    end)
end

-- Moisture Meter: apparently not a subclass of widget/badge
if (GLOBAL.IsDLCEnabled(1)) == true then
    AddClassPostConstruct("widgets/moisturemeter", function(self)
        self.num:SetColour(white[1], white[2], white[3], configNumberAlpha)

        self.deltaColorChange = configDeltaColorChange

        local oldActivate = self.Activate
        function self:Activate()
            local result = oldActivate(self)

            self.num:Show()
            self:StartColorChangeTask()

            return result
        end

        local oldDeactivate = self.Deactivate
        function self:Deactivate()
            local result = oldDeactivate(self)

            self.num:Hide()
            self.colorChangeTask:Cancel()

            --this is hackish but sometimes it doesn't go away right away
            self.inst:DoTaskInTime(.1, function(inst)
                self.num:Hide()
            end)

            return result
        end

        local oldOnLoseFocus = self.OnLoseFocus
        function self:OnLoseFocus()
            local result = oldOnLoseFocus(self)

            self.num:Show()

            return result
        end

        function self:StartColorChangeTask()
            if configDeltaColorChange == "no" then return end

            self.colorChangeTask = self.inst:DoPeriodicTask(0.25, function(inst)
                local rate = self.owner.components.moisture:GetDelta()

                if rate == 0 then return end

                local newColor = white
                if rate < 0 then
                    newColor = green
                elseif rate > 0 then
                    newColor = red
                end

                self.num:SetColour(newColor[1], newColor[2], newColor[3], configNumberAlpha)

                if self.colorRevertTask ~= nil then
                    self.colorRevertTask:Cancel()
                end

                self.colorRevertTask = self.inst:DoTaskInTime(.5, function(inst)
                    self.num:SetColour(white[1], white[2], white[3], configNumberAlpha)
                end)
            end)
        end
    end)
end
