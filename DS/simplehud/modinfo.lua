name = "Simple HUD +1"
description = "A very simple mod that shows your stats all the time. Hovering your mouse over the stats shows the stat's maximum capacity."
author = "Mouse & Plaidman"
version = "1.3p"

forumthread = ""

api_version = 6

icon_atlas = "shud.xml"
icon = "shud.tex"

dont_starve_compatible = true
reign_of_giants_compatible = true

configuration_options = {
	{ 
        name = "displayTemperature",
		label = "Clock Temperature",
		options = {
			{description = "Yes", data = true},
			{description = "No", data = false},
		},
		default = true
	},
	{
		name = "numberAlpha",
		label = "Number Visibility",
		options = {
			{description = "100%", data = 1},
			{description = "75%", data = .75},
			{description = "50%", data = .5},
			{description = "25%", data = .25},
			{description = "10%", data = .1},
		},
		default = 1
	},
	{
		name = "deltaColorChange",
		label = "Colors for +/-",
		options = {
			{description = "Yes", data = "yes"},
            {description = "Not Hunger", data = "notHunger"},
			{description = "No", data = "no"},
		},
		default = "notHunger"
	},
}
